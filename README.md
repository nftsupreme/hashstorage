# DATA-STORAGE SMART CONTRACTS IPFS

## instalations

> npm init -y // node package json
> truffle init // contracts, migrations, truffle-config.js
> git init // .gitignore
> npm install dotenv //.env files

add this to the .gitignore file

// # mine configuration

MY-README.md
prod-env

// # mine configuration

add the particular configuration of truffle.config

create .env file

# npm.runkit.com

// save data in ipfs
const IPFS = require('ipfs-mini');
const ipfs = new IPFS({host: 'ipfs.infura.io', port: 5001, protocol: 'https'});
const randomData = "8803cf48b8805198dbf85b2e0d514320"; // random bytes for testing
ipfs.add(randomData, (err, hash) => {
if (err) {
return console.log(err);
}

console.log("HASH:", hash); // "Qmaj3ZhZtHynXc1tpnTnSBNsq8tZihMuV34wAvpURPZZMs"
});

// load data in ipfs // WARNING no funciona bien la libreria buscar otar o solucion
const IPFS = require('ipfs-mini');
const ipfs = new IPFS({host: 'ipfs.infura.io', port: 5001, protocol: 'https'});
const hash = "Qmaj3ZhZtHynXc1tpnTnSBNsq8tZihMuV34wAvpURPZZMs";
ipfs.cat(hash, (err, data) => {
if (err) {
return console.log(err);
}

console.log("DATA:", data);
});

## EXPLAINING

- solucion 1: poner el array generado con makeArrayNumbers(1, 55550) en el smartcontract directamente,
  ver si permite un array tan grande sin exceder el máximo del smart contract 24Kb.

- solución 2: generar el array, subirlo al servidor ipfs, y crear una comunicación de lectura sobre la data, es decir leer el array, para ello he generado ipfs-manager y un smart contract de ejemplo.
  y a partir de esa lectura generar el array predefinido con esos datos uint256[55550] arrayCards, para
  ell oconociendo a solidity, habrá que ejecutar un loop para rellenarlo

- solucion 3: crear una función inicial esternal onlyOwner, en el smart contract, que solo haga el for desde n1 hasta n2, después en web3 un script, que genere diferentes transacciones para darle a esa función un valor entre 1 y 1111, entre 1112 y 2222, ... hasta llegar a 55550, el valor de los indices generados irán a un array predefinido uint256[55550] arrayCards; 0->1,1->2,55549->55550
  ejecutar el script de web3 con diferentes tx, hasta qu se genere por completo el array.
  Habría que mirar cual es el máximo gas de computo permitido por tx. Esto es crítico si falla algo,
  se jode el smart contract.

Probar 1, 2, 3, del más fácil al más difícil ,el primero que funcione aceptamos.
