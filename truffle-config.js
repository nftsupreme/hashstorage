require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');

// const INFURA_KEY = process.env.INFURA_KEY_BSCTESTNET;

// const INFURA_KEY = process.env.INFURA_KEY_ROSPTENTESTNET;

const mnemonic = process.env.MNEMONIC_TESTNET_MUMBAI; //

module.exports = {
	// contracts_directory: './src/contracts'; // needed for web3.js with react import instanceNFTSupremeContract from 'contracts/SupremeNFT.json';

	/**
	 * Networks define how you connect to your ethereum client and let you set the
	 * defaults web3 uses to send transactions. If you don't specify one truffle
	 * will spin up a development blockchain for you on port 9545 when you
	 * run `develop` or `test`. You can ask a truffle command to use a specific
	 * network from the command line, e.g
	 *
	 * $ truffle test --network <network-name>
	 */

	networks: {
		// Useful for testing. The `development` name is special - truffle uses it by default
		// if it's defined here and no other network is specified at the command line.
		// You should run a client (like ganache-cli, geth or parity) in a separate terminal
		// tab if you use this network and you must also set the `host`, `port` and `network_id`
		// options below to some value.
		//
		development: {
			host: '127.0.0.1', // Localhost (default: none)
			port: 7545, // Standard Ethereum port (default: none)
			network_id: '*', // Any network (default: none)
		},
		// ####################################################################################
		// TESTNETS
		// ####################################################################################
		POLYGON_TESTNET_MUMBAI: {
			// ????? https://docs.polygon.technology/docs/develop/truffle/
			provider: () => new HDWalletProvider(mnemonic, `https://rpc-mumbai.maticvigil.com`),
			network_id: 80001,
			confirmations: 2,
			timeoutBlocks: 200,
			skipDryRun: true,
		},

		// ropsten: {
		// 	provider: () => new HDWalletProvider(mnemonic, `https://ropsten.infura.io/v3/${INFURA_KEY_ROPSTEN}`),
		// 	network_id: 3,
		// 	skipDryRun: true,
		// },
		// rinkeby: {
		// 	provider: () => new HDWalletProvider(mnemonic, `https://rinkeby.infura.io/v3/${INFURA_KEY_RINKEBY}`),
		// 	network_id: 4,
		// 	gas: 4500000,
		// 	gasPrice: 20000000000,
		// 	skipDryRun: true,
		// },
		// BSC_TESTNET: {
		// 	provider: () => new HDWalletProvider(mnemonic, `https://data-seed-prebsc-1-s1.binance.org:8545/`),
		// 	network_id: 97,
		// 	gas: 4500000,
		// 	gasPrice: 20000000000,
		// 	skipDryRun: true,
		// },
		// ####################################################################################
		// MAINNETS
		// ####################################################################################
		// BINANCE_SMART_CHAIN MAINNET
		// BSC_MAINNET: {
		// 	provider: () => new HDWalletProvider(mnemonic, `https://bsc-dataseed.binance.org/`),
		// 	network_id: 56,
		// 	gas: 4500000,
		// 	gasPrice: 20000000000,
		// 	skipDryRun: true,
		// },
		// POLYGON_MAINNET_MATIC: {
		// 	provider: () => new HDWalletProvider(mnemonic, `https://rpc-mainnet.matic.network`),
		// 	network_id: 137,
		// 	gas: 4500000,
		// 	gasPrice: 20000000000,
		// 	skipDryRun: true,
		// },
	},

	// Set default mocha options here, use special reporters etc.
	mocha: {
		// timeout: 100000
	},

	// Configure your compilers
	compilers: {
		solc: {
			version: '0.8.7', // Fetch exact version from solc-bin (default: truffle's version)
			// docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
			settings: {
				// See the solidity docs for advice about optimization and evmVersion
				optimizer: {
					enabled: true,
					runs: 200,
				},
				// outputSelection: {
				// 	'*': {
				// 		'*': ['evm.bytecode', 'evm.deployedBytecode', 'devdoc', 'userdoc', 'metadata', 'abi'],
				// 	},
				// },
				// metadata: {
				// 	useLiteralContent: true,
				// },
				// libraries: {},
			},
		},
	},

	// Truffle DB is currently disabled by default; to enable it, change enabled: false to enabled: true
	//
	// Note: if you migrated your contracts prior to enabling this field in your Truffle project and want
	// those previously migrated contracts available in the .db directory, you will need to run the following:
	// $ truffle migrate --reset --compile-all

	db: {
		enabled: false,
	},
	plugins: ['truffle-contract-size'],
};
