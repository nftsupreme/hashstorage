var fs = require('fs');
var path = require('path');
var ruta = path.join(__dirname, 'result');
var file = path.join(ruta, 'arrayNumbers.json');

async function makeArrayBetweenNumbers(n1, n2) {
	let arrayResult = [];
	for (i = n1; i <= n2; i++) {
		arrayResult.push(i);
	}
	let dirExists = await fs.existsSync(ruta);
	if (!dirExists) {
		fs.mkdirSync(ruta);
	}

	// escribir en el fichero, si existe lo borra
	await fs.writeFile(
		file,
		JSON.stringify(arrayResult),
		{
			encoding: 'utf8', // utf8 es el valor por defecto
			//   mode: "0o666", // son los permisos, este valor puede variar dependiendo si te encuentras en Linux o Windows,
			flag: 'w', // w indica que el archivo se debe crear si no existe o su contenido se debe reemplazar si existiera
		},
		(err) => {
			if (err) {
				throw err;
			} else {
				console.log(true);
			}
		}
	);
	return arrayResult;
}

module.exports = makeArrayBetweenNumbers;
