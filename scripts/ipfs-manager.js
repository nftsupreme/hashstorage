var fs = require('fs');
var makeArrayNumbers = require('./makeArrayNumbers.js');
// var path = require('path');
var path = './result/';
var file = path + 'arrayNumbers.json';

const IPFS = require('ipfs-mini');
const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });
const randomData = '8803cf48b8805198dbf85b2e0d514320'; // random bytes for testing
var arrayNumbers;

async function makeNumbers() {
	finish = await makeArrayNumbers(1, 10);
	console.log(finish);
}

// save data in ipfs // change randomData for arrayNumbers
function saveDataIPFS() {
	ipfs.add(randomData, (err, _hash) => {
		if (err) {
			return console.log(err);
		}

		console.log('HASH:', _hash); // "Qmaj3ZhZtHynXc1tpnTnSBNsq8tZihMuV34wAvpURPZZMs"
	});
}
// WARNING falla ipfs-mini, no está actualizado, en la forma que se accede a ipfs hoy día, buscar otra libreria o solucion
function loadDataFromIPFS() {
	const hash = 'Qmaj3ZhZtHynXc1tpnTnSBNsq8tZihMuV34wAvpURPZZMs';
	ipfs.cat(hash, (err, data) => {
		if (err) {
			return console.log(err);
		}

		console.log('DATA:', data);
	});
}
makeNumbers();
// saveDataIPFS();
// loadDataFromIPFS();
